/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Ejercicio;

import Varios.ArchivoLeerURL;

/**
 *
 * @author Acer
 */
public class Laberinto {

    /**
     * @param args the command line arguments
     */
    short matriz[][];
    Object fila1;
    
    public Laberinto(String url){
        this.leerUrlLaberinto(url);
    }
    
    private void leerUrlLaberinto(String url){
         
       ArchivoLeerURL file = new ArchivoLeerURL(url);
       Object v[] = file.leerArchivo();

        // El tamaño de las filas es v.length
        
        int columnas = v[0].toString().split(";").length;
        
        /* Con el split creamos un nuevo arreglo con los elementos de v[0] y nuestro separador será ";"
        v[0] = {1;1;2;3}  < String
        ej: [1,1,2,3] -> .length = 4 = columnas.
        */
        
        this.matriz = new short[v.length][columnas];
        
        for(int i=0; i<v.length; i++){
            String fila[]= v[i].toString().split(";");
           
            for(int j=0; j<fila.length; j++){
                this.matriz[i][j]= Short.parseShort(fila[j]);
            }
                 
        }
        
        
     }
    
    public short[][] getMatriz()
    {
        return this.matriz;
    }
    
    public String getCamino(int filaTeseo, int colTeseo, int filaMinTauro, int colMinTauro ){
           
        if( minotauroMuerto(filaMinTauro, colMinTauro) && encontroSalida(filaTeseo, colTeseo))
        {
            return "(" + filaTeseo + "," + colTeseo + ")";
        }
        

        
    }
         
    private boolean minotauroMuerto(int filaMinTauro, int colMinTauro){     
        return matriz[filaMinTauro][colMinTauro] == -100;   
    }
    
    private boolean encontroSalida(int filaTeseo, int colTeseo){
         return matriz[filaTeseo][colTeseo] == 1000;
    }
    
    private boolean existeCamino(int filaTeseo, int colTeseo){
        
        if(filaTeseo+1 <= matriz.length-1) //hacia abajo
            return matriz[filaTeseo+1][colTeseo]==0;

        if(colTeseo+1 <= matriz[0].length-1) //hacia la derecha
            return matriz[filaTeseo][colTeseo+1]==0;
       
        if(colTeseo-1 >=0) //hacia izquierda
            return matriz[filaTeseo][colTeseo-1]==0;
        
        if(filaTeseo-1 >= 0) // hacia arriba
            return matriz[filaTeseo-1][colTeseo]==0; 
   
        
     return false;
    } 
    
    
    

    
}
